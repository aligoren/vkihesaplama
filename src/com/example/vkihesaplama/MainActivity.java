package com.example.vkihesaplama;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final EditText boy=(EditText)findViewById(R.id.boy);
		final EditText kilo=(EditText)findViewById(R.id.kilo);
		final TextView sonuc=(TextView)findViewById(R.id.sonuc);
		final TextView sonucVerisi=(TextView)findViewById(R.id.sonucVerisi);
		Button hesaplaButton = (Button)findViewById(R.id.hesaplaButon);
		Button hakkindaButton = (Button)findViewById(R.id.hakkindaButon);
		hakkindaButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				SonucMesajlari progHak = new SonucMesajlari();
				sonucVerisi.setText(progHak.programHakkinda);
			}
		});
		
		hesaplaButton.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
			try
			{
				
				double boyDegeri = Double.parseDouble(boy.getText().toString());
				double kiloDegeri = Double.parseDouble(kilo.getText().toString());
				double endex = kiloDegeri / (boyDegeri * boyDegeri);
				sonuc.setText(String.valueOf("VK� Degeriniz:\n" + Math.round(endex)));
				
				SonucMesajlari mesaj = new SonucMesajlari();
				
				if(endex > 0 && endex<=18.4)
				{
					sonucVerisi.setText(mesaj.sonuc + mesaj.zayif + mesaj.tahmin[0]);
				}
				else if(endex > 18.4 && endex <= 24.9)
				{
					sonucVerisi.setText(mesaj.sonuc + mesaj.normal + mesaj.tahmin[1]);
				}
				else if(endex > 25 && endex <= 29.9)
				{
					sonucVerisi.setText(mesaj.sonuc + mesaj.kilolu + mesaj.tahmin[2]);
				}
				else if(endex > 30 && endex <= 34.9)
				{
					sonucVerisi.setText(mesaj.sonuc + mesaj.birinciSinifObez + mesaj.tahmin[3]);
				}
				else if(endex > 35 && endex <= 44.9)
				{
					sonucVerisi.setText(mesaj.sonuc + mesaj.ikinciSinifObez + mesaj.tahmin[4]);
				}
				else if(endex >= 50)
				{
					sonucVerisi.setText(mesaj.sonuc + mesaj.ucuncuSinifObez + mesaj.tahmin[5]);
				}
				
			} catch (Exception e){
				sonuc.setText("L�TFEN GER�EK B�R DE�ER G�R�N�Z");
			}
				
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	

}
