package com.example.vkihesaplama;

public class SonucMesajlari {

	String sonuc = "SONU�:\n\n";
    
    String[] tahmin = {"\n\nDurum: Zay�f", "\n\nDurum: Normal",
        "\n\nDurum: Kilolu", "\n\nDurum: 1. Derece Obez",
          "\n\nDurum: 2. Derece Obez", "\n\nDurum: A��r� Obez"};
    
    String zayif = "\n\nOneri:\n\nBoyunuza g�re uygun a��rl�kta olmad���n�z�,"
                      + " zay�f oldu�unuzu g�sterir. Zay�fl�k, baz� hastal�klar i�in risk olu�turan "
                      + "ve istenmeyen bir durumdur. Boyunuza uygun"
                      + " a��rl��a eri�meniz i�in yeterli ve dengeli beslenmeli, "
                      + "beslenme al��kanl�klar�n�z� geli�tirmeye �zen g�stermelisiniz."
                      + "\n"
                      + "\nEn k�sa s�rede doktorunuza ba�vurmay� unutmay�n."
                      + "Daha sa�l�kl� bilgileri ondan alman�z gerekir";
    
    String normal = "\n\n�neri:\n\nBoyunuza g�re uygun a��rl�kta oldu�unuzu g�sterir. \nYeterli ve"
            + " dengeli beslenerek ve d�zenli fiziksel aktivite yaparak"
            + " bu a��rl���n�z� korumaya �zen g�steriniz.";
    
    String kilolu = "\n\n�neri:\n\nBoyunuza g�re v�cut a��rl���n�z�n fazla oldu�unu g�sterir."
            + " \nFazla kilolu olma durumu gerekli �nlemler al�nmad��� "
            + "takdirde pek �ok hastal�k i�in"
            + " risk fakt�r� olan obeziteye (�i�manl�k) yol a�ar.";
    
    String birinciSinifObez = "\n\n�neri:\n\nBoyunuza g�re v�cut a��rl���n�z�n fazla oldu�unu "
            + "bir ba�ka deyi�le �i�man oldu�unuzun "
            + "bir g�stergesidir. �i�manl�k, kalp-damar hastal�klar�, diyabet,"
            + " hipertansiyon v.b. kronik hastal�klar i�in risk fakt�r�d�r. "
            + "Bir sa�l�k kurulu�una ba�vurarak hekim / diyetisyen kontrol�nde"
            + " zay�flayarak normal a��rl��a inmeniz sa�l���n�z a��s�ndan �ok �nemlidir."
            + " L�tfen, sa�l�k kurulu�una ba�vurunuz.";
    
    String ikinciSinifObez = "\n\n�neri:\n\nBoyunuza g�re v�cut a��rl���n�z�n fazla oldu�unu bir ba�ka deyi�le �i�man oldu�unuzun"
            + " bir g�stergesidir. �i�manl�k, kalp-damar hastal�klar�,"
            + " diyabet, hipertansiyon v.b. kronik hastal�klar i�in risk fakt�r�d�r."
            + " Bir sa�l�k kurulu�una ba�vurarak hekim / diyetisyen kontrol�nde zay�flayarak "
            + "normal a��rl��a inmeniz sa�l���n�z a��s�ndan �ok �nemlidir. "
            + "L�tfen, sa�l�k kurulu�una ba�vurunuz.";
    
    String ucuncuSinifObez = "\n\n�neri:\n\nBoyunuza g�re v�cut a��rl���n�z�n fazla oldu�unu bir ba�ka deyi�le �i�man"
            + " oldu�unuzun bir g�stergesidir. �i�manl�k, kalp-damar hastal�klar�, diyabet,"
            + " hipertansiyon v.b. kronik hastal�klar i�in risk fakt�r�d�r."
            + " Bir sa�l�k kurulu�una ba�vurarak hekim / diyetisyen kontrol�nde zay�flayarak normal"
            + " a��rl��a inmeniz sa�l���n�z a��s�ndan �ok �nemlidir. "
            + "L�tfen, sa�l�k kurulu�una ba�vurunuz.";
    
    String programHakkinda = "\n\nHakk�nda:\n\n Ali G�REN taraf�ndan yaz�lm��t�r. " +
    		"Telif hakk� falan yoktur. Kodlar github �zerinde yer alacakt�r";
}
